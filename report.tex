\RequirePackage{plautopatch}
\documentclass[luatex, unicode, 11pt, a4paper]{ltjsarticle}

%%% LuaLaTeX関係。
\usepackage{fontspec}
\usepackage{luatexja}
\usepackage{luatexja-otf}
\usepackage[deluxe]{luatexja-fontspec}
\setmainjfont{IPAexMincho}[
  BoldFont=MogaMincho-Bold
]
\setsansjfont{IPAexGothic}[
  BoldFont=MigMix-2P-Bold
]
\usepackage{unicode-math}
\unimathsetup{math-style=TeX, bold-style=ISO}
%\usepackage{luacode}

%%% その他のパッケージ。
\input{settings.tex}
\usepackage{tikz-lace}
\usepackage[backend=biber, style=numeric, giveninits=true, sorting=nyt]{biblatex}
\addbibresource{bibliography/bibliography.bib}
\AtEveryBibitem{%
  % \clearfield{title}
  % \clearlist{publisher}
  \clearname{editor}
  \clearfield{isbn}
  \clearfield{issn}
  % \clearlist{location}
  % \clearfield{doi}
  \clearfield{url}
  \clearfield{urlyear}
}

%%% 文章の書式設定。
\title{最近接有向パーコレーションに対するレース展開}
\author{上島芳倫 (KAMIJIMA Yoshinori)\thanks{Postdoctoral Fellow, Mathematics Division, National Center for Theoretical Sciences, Taiwan}}
\date{}

%\renewcommand{\emph}{\textbf}
%(
\renewcommand{\thefootnote}{\arabic{footnote})}
\pagestyle{empty}
\allowdisplaybreaks[2]

\begin{document}

\maketitle

\begin{abstract}
  \noindent
  有向パーコレーションは感染症の細菌の生存・死滅を記述するモデルである．数学的には，格子グラフ $\mathbb{L}^d$ と非負整数全体 $\mathbb{Z}_+$ の直積 $\mathbb{L}^d\times\mathbb{Z}_+$ の辺集合に対して，開・閉なる状態をランダムに与えることで定義される．開の辺で繋がった頂点数の期待値には冪乗則 $(p_\mathrm{c}-p)^{-\gamma}$ が予想されており，特に $d>4$ ならば $\gamma=1$ だと信じられている．講演者らはこれを単純立方格子では $d+1=183+1$ で，体心立方格子では $d+1=9+1$ で証明したので，それを紹介する．
  本講演は陳隆奇教授（國立政治大學，台湾）と半田悟氏 (atama plus Inc.) との共同研究~\cite{chen2021Meanfield}に基づく．
\end{abstract}

\section{導入}\label{sec:introduction}

1957年にBroadbendとHammersley~\cite{broadbent1957Percolation}は有向パーコレーションと呼ばれる確率論的な模型を導入した．
これは多孔質材料への水の浸透現象や，感染症の細菌が生存・死滅するまでの過程を記述するモデルの一種である．
例えば，単純立方格子\footnote{%
  固体物理学で専ら使われる呼び方．数学では「整数格子」などと呼び，あまりこのような用語は使わないだろう．本稿では，後述の体心立方格子\――これも固体物理学の用語\――に合わせて，$\mathbb{Z}^d$ もこのように呼ぶ．%
} $\mathbb{Z}^d$ 上に感染する対象が配置されているとしよう．
非負整数の時間 $\opTime\defeq\mathbb{N}\sqcup\{0\}$ を考え，時刻 $0$ で原点 $o$ の対象のみ感染していたとする．
さらに，時刻が $1$ 進むたびに前の時刻で感染していた者からランダムな確率 $p\in\interval{0}{1}$ で感染が起こるとする．
このとき，確率 $p$ を変化させると，それに伴って時刻 $\infty$ までの感染者数（クラスター・サイズ）が変化する．
自明な場合として，$p=0$ では時刻 $1$ の時点で細菌が死滅（感染者数が $1$）し，$p=1$ では時刻 $\infty$ までの全ての者が感染（感染者数が $\infty$）する．
実は，$d\geq 1$ ならば或る非自明な確率 $0<p_\mathrm{c}<1$ が存在して，任意の $p>p_\mathrm{c}$ で感染者数の期待値が発散することが知られている．
以上のように，パラメータ（今の場合は確率 $p$）を変化させたときに，モデルに対して定義された関数が特異な振る舞いをすることを相転移と呼ぶ．

相転移は統計物理学に於ける重要な研究対象の一つである．
相転移の身近な例の一つとして，水の三態が挙げられる．
水という物質は分子 ($\ce{H2O}$) の集まりであることはよく知られているものの，原子論の観点から液体と気体を区別することは非自明な問題である（力学的に分子一つ一つが互いに相互作用を及ぼし合って運動している，という観点では液体も気体も同じである）．
この意味で，相転移を数学的に研究することには価値がある．

有向パーコレーションでは，前述のようにクラスター・サイズの期待値を見ることで，相転移が起こることがわかる．
特に，各相の境 $p_\mathrm{c}$ を臨界点と呼び，その周りではクラスター・サイズなどの関数に対して冪乗則が成り立つと予想されている．
このような現象を臨界現象という．
具体的には，或る正の指数 $\gamma$ が存在して，$p\uparrow p_\mathrm{c}$ で漸近的にクラスター・サイズの期待値が $(p_\mathrm{c} - p)^{-\gamma}$ のように振る舞うと予想されている．
この指数を臨界指数という．
一般に臨界指数は複雑な実数値を取るものの，空間次元が十分大きい $d>d_\mathrm{c}=4$ ときには $\gamma=1$ に退化すると予想されている．
これらの値は相互作用の無いモデルの臨界指数に対応することが知られており，平均場臨界指数と呼ぶ．
平均場臨界指数への退化が起こるぎりぎりの次元 $d_\mathrm{c}$ を（上部）臨界次元と呼ぶ．

水の例でいうと，その温度と圧力に関する相図を見たときに，気相と液相を区別する線が途中で切れている点がある．
この点が臨界点である．
臨界点の近傍では，気相と液相の体積密度の差に対して，前述のような冪乗則が実験的に観測されている．
特に興味深いのは，水に限らず様々な物質でも，臨界点の値が変わる一方で臨界指数は同じ値になることである．
すなわち，臨界指数はモデルの詳細（格子の種類など）によらず，対称性や次元にのみ依存すると予想されている．
このような臨界指数の普遍性を解明することは統計物理学に於ける重要な課題の一つである．

本稿では，最近接有向パーコレーションに対する高次元臨界現象を取り扱う．
本研究の目的は臨界次元の予想 $d_\mathrm{c}=4$ を数学的に証明することである．
高次元臨界現象の解析にはレース展開と呼ばれる手法が使われてきた\cite{nguyen1993Triangle,nguyen1995Gaussian}ので，本研究でもそれを利用する．
また，本研究では単純立方格子以外に体心立方格子を導入する．
詳しくは後述するが，体心立方格子上のランダム・ウォークを考えると，その再帰確率が小さく評価しやすいので，レース展開の展開係数の収束性を示す際に役に立つ．
結果として（臨界次元の予想値よりも大きいものの）単純立方格子 $\mathbb{Z}^d\times\opTime$ 上では $d + 1\geq 183 + 1$ で，体心立方格子 $\mathbb{L}^d\times\opTime$ 上では $d + 1\geq 9 + 1$ で平均場臨界現象への退化を示せた．
先行研究\cite{nguyen1993Triangle}では，$\mathbb{Z}^d\times\opTime$ 上の最近接有向パーコレーションに対して平均場臨界指数への退化を示しているものの「十分高次元 $d\gg 4$」と書いてあるのみで，レース展開の解析が成り立つ具体的な次元を特定していなかった．
以下では，その主張の数学的な定式化と証明について紹介する．

\section{モデルの定義}

\subsection{格子の定義とランダム・ウォーク}

まず，体心立方格子 (body-centered cubic lattice) $\mathbb{L}^d$ を定義する．
最近接点の集合を $\mathscr{N}^d_{\mathrm{BCC}} = \{x = (x_1, \dots, x_d)\in\mathbb{Z}^d \mid \prod_{i=1}^{d}\abs{x_i} = 1\}$ とする．
体心立方格子 $\mathbb{L}^d$ とは，原点を含み $o=(0, \dots, 0)\in\mathbb{L}^d$，かつ最近接点の集合 $\mathscr{N}^d$ を平行移動することによって生成されるグラフ\footnote{%
  グラフ理論の観点からはグラフ $G=(V, E)$ とその頂点集合 $V$ とを厳密に区別するべきであるものの，本稿では便宜上それらを区別せずに ``$x\in\opSpace$'' のように書く．$\mathscr{N}^d$ の平行移動で定義したため，辺集合は $\{\{x, y\} \subset \mathbb{Z}^d \mid y - x\in\mathscr{N}^d\}$ で与えられることにも注意されたい．%
}である．
同様に，単純立方格子 (simple cubic lattice) $\mathbb{Z}^d$ とは，原点を含み，かつ最近接点の集合 $\mathscr{N}^d_{\mathrm{SC}} = \{x = (x_1, \dots, x_d)\in\mathbb{Z}^d \mid \sum_{i=1}^{d}\abs{x_i} = 1\}$ を平行移動することによって生成されるグラフと見なす．
以下では，両方のグラフを纏めて扱うために，$\opSpace = \mathbb{Z}^d$ or $\mathbb{L}^d$ および $\mathscr{N}^d = \mathscr{N}^d_{\mathrm{SC}}$ or $\mathscr{N}^d_{\mathrm{BCC}}$ と書く．

つぎに，各 $x\in\opSpace$ に対して，$\opSpace$ 上の $d$ 次元ランダム・ウォークの遷移確率を\footnote{%
  $\indicator{\set{\bullet}}$ は命題 $\set{\bullet}$ が真ならば $1$，偽ならば $0$ を返す定義関数．%
}\footnote{%
  $\card{A}$ は集合 $A$ の濃度を表す．%
}
\[
  D(x) \defeq \frac{1}{\card{\mathscr{N}^d}} \indicator{\set{x\in\mathscr{N}^d}}
\]
で定義する．
これを用いて，ランダム・ウォークのループ ($i=1$) およびバブル $(i=2)$ をそれぞれ
\begin{equation}
  \label{eq:def-rwQuantities}
  \varepsilon_i^{(\nu)} \defeq \sum_{n=\nu}^{\infty} D^{\conv 2n}(o) \times
  \begin{cases}
    1 & [i = 1],\\
    \left(n - \nu + 1\right) & [i = 2]
  \end{cases}
\end{equation}
で定義する\footnote{%
  関数 $f, g\colon \opSpace\to\mathbb{R}$ に対して，$(f\conv g)(x) = \sum_{y\in\opSpace} f(y) g(x - y)$ は畳み込みを表す．また，関数の肩に乗せたものは $n$ 重畳み込み $f^{\conv n}(x) = \sum_{y\in\opSpace} f^{\conv (n-1)}(y) f(x - y)$ を意味する．%
}．
この内，ループは「原点出発のランダム・ウォークが $2\nu$ 歩費やして戻る確率」を意味する．
特に，$\opSpace = \mathbb{L}^d$ のときには，最近接点の定義によって，$d$ 次元ランダム・ウォークの遷移確率が $1$ 次元ランダム・ウォークの遷移確率の積で表される\footnote{$\KroneckerDelta{\bullet}{\bullet}$ はKroneckerデルタ．}：$D(x) = \prod_{j=1}^{d} (\KroneckerDelta{\abs{x_j}}{1} / 2)$．
体心立方格子のこの性質はランダム・ウォーク量\eqref{eq:def-rwQuantities}の上界を評価する際に，Stirlingの公式を使えるため便利である．
本稿では，紙面の都合上その計算の詳細を省き，表~\ref{tbl:numericalValues-rw-bcc}に数値計算の結果\footnote{%
  この表の数値を得るために（および下記のbootstrap argumentを確認するために）Pythonのプログラムを作成した．ソースコードは\url{https://gitlab.com/ykami/comp-lace_for_op}から取得できる．%
}を示すに留める．
また，$\opSpace=\mathbb{Z}^d$ のときには，第1種変形Bessel関数と数値積分を用いることで評価できる\cite[Appendix~A]{kamijima2021Meanfield}．

\begin{table}[hbp]
  \caption{体心立方格子に於けるランダム・ウォーク量の上界．}
  \centering
  \begin{tabular}{|c|c|c|c|c|}
    \hline
    $d$ & $\varepsilon_1^{(1)}$ & $\varepsilon_1^{(2)}$ & $\varepsilon_2^{(1)}$ & $\varepsilon_2^{(2)}$ \\
    \hline
    \num{3}  & \num{3.932160e-01} & \num{2.682160e-01} & $\infty$ & $\infty$\\
    \num{4}  & \num{1.186367e-01} & \num{5.613669e-02} & $\infty$ & $\infty$\\
    \num{5}  & \num{4.682556e-02} & \num{1.557556e-02} & \num{1.125787e-01} & \num{6.575313e-02}\\
    $\vdots$  & $\vdots$ & $\vdots$ & $\vdots$ & $\vdots$\\
    \num{9}  & \num{2.143604e-03} & \num{1.904782e-04} & \num{2.410377e-03} & \num{2.667729e-04}\\
    \hline
  \end{tabular}
  \label{tbl:numericalValues-rw-bcc}
\end{table}

\subsection{有向パーコレーション}

まず，時空間について説明する．
本稿では，直積集合 $\opSpaceTime$ を時空間と呼び，その元を太字で $\symbf{x}=(x, t)\in\opSpaceTime$ と表す．
%空間成分 $x=(x_1, \dots, x_d)\in\mathbb{G}^d$ は通常のフォントである．
以下では，特に断らずに時空間の点 $\symbf{x}$ の空間成分を通常のフォント $x=(x_1, \dots, x_d)\in\opSpace$ に対応させ，また $\symbf{x}$ の時間成分を $\tau(\symbf{x})$ と記す．
時空間のボンドを順序付けられた組 $((x, t), (y, t+1)) \in (\opSpaceTime)^2$ で定義する．
つぎに，有向パーコレーションの定義を述べる．
有向パーコレーションとは，各ボンド $(\symbf{x}, \symbf{y})$ と $p\in\interval{0}{\norm{D}_\infty^{-1}}$ に対して\footnote{%
  $f\colon \opSpace\to\mathbb{R}$ に対して，$\norm{f}_\infty=\sup_{x\in\opSpace}\abs{D(f)}$．また，第\ref{sec:introduction}節では $p$ を確率としていたが，以下では $D$ と掛け合わせた $pD(\bullet)$ が確率である．%
}，$(\symbf{x}, \symbf{y})$ が開である確率を $q_p(\symbf{y} - \symbf{x})\defeq pD(y - x)\KroneckerDelta{\tau(\symbf{y}) - \tau(\symbf{x})}{1}$ で，閉である確率を $1 - q_p(\symbf{y} - \symbf{x})$ で与えるモデルである．
ただし，各々のボンドに対して，開と閉の状態は独立に与える．
以上のようにして与えた，ボンドに関するBernoulli確率変数の結合分布を $\mathbb{P}_p$，さらにこの確率測度に関する期待値を $\mathbb{E}_p$ と書く．

有向パーコレーションの二点関数と感受率（クラスター・サイズ）を次のように定義する．
時空間の2点 $\symbf{x}$ と $\symbf{y}$ が連結であるとは，時空間のボンドの列 $\{\symbf{b}_1, \dots, \symbf{b}_{\tau(\symbf{y}) - \tau(\symbf{x})}\}$ が存在して全ての $\symbf{b}_i$ ($i=1, \dots, \tau(\symbf{y}) - \tau(\symbf{x})$) が開であることをいい，$\symbf{x}\rightarrow\symbf{y}$ と書く．
このとき，$\symbf{x}=(x, t)\in\opSpaceTime$ に対して，有向パーコレーションの二点関数 $\varphi_p$ を
\begin{equation*}
  \varphi_p(\symbf{x})
  = \mathbb{P}_p(\symbf{o}\rightarrow\symbf{x})
\end{equation*}
によって定義する．ここで，$\symbf{o}\defeq (o, 0)$ である．
また，$\mathcal{C}(\symbf{o}) = \set{\symbf{x}\in\opSpaceTime | \symbf{o}\rightarrow\symbf{x}}$ を（原点を含む）クラスターと呼ぶ．
これを用いて，有向パーコレーションの感受率 $\chi_p$ と臨界点 $p_\mathrm{c}$ を\footnote{%
  集合 $C$ に対して，$\card{C}$ は集合の濃度．%
}
\begin{align*}
  \chi_p &= \mathbb{E}_p\bigl[\card{\mathcal{C}(\symbf{o})}\bigr] = \sum_{\symbf{x}\in\opSpaceTime} \varphi_p(\symbf{x}),&
  p_\mathrm{c} &= \sup\Set{p\in\interval{0}{\norm{D}_\infty^{-1}} | \chi_p < \infty},
\end{align*}
によって定義する．
臨界指数 $\gamma$ は感受率の臨界点近傍 ($p \uparrow p_\mathrm{c}$) での漸近的な振る舞いを特徴付けるものとして，$\chi_p \asymp (p_\mathrm{c} - p)^{-\gamma}$ で定義する．
ここで，$x\to a$ で $f(x) \asymp g(x)$ とは，$\order{g(x)} \leq f(x) \leq \order{g(x)}$ となることである．

\subsection{Fourier-Laplace変換}

主結果を述べるために必要となるので，Fourier-Laplace変換とそれに関連する結果について列挙する．
二点関数 $\varphi_p(x, t)$ の $x$ に関するFourier変換を $\Phi_p(k; t) = \sum_{x\in\opSpace}\varphi_p(x, t)\Napier^{\imag k\cdot x}$ とすると，
$\{\log\Phi_p(k; t) / t\}_{t=1}^{\infty}$ が劣加法的数列になることによって，
極限 $m_p^{-1}=\lim_{t\uparrow\infty}\Phi_p(k; t)^{1/t}$ が存在する~\cite[Appendix.~II]{grimmett1999Percolation}．
ただし，$k\in\mathbb{T}^d=\interval{-\pi}{\pi}^d$ はトーラスの点であり，$x\cdot k$ はEuclid内積である．
$p<p_\mathrm{c}$ では $m_p<1$ であり，特に $m_{p_\mathrm{c}} = 1$ が成り立つ~\cite{nguyen1995Gaussian}．
このように定義すると，$m_p$ はLaplace変換
\begin{equation*}
  \flt\varphi_p(k, z)
  \defeq \sum_{t\in\opTime}\Phi_p(k; t) z^t
  = \sum_{(x, t)\in\opSpaceTime} \varphi_p(x, t) \Napier^{\imag k\cdot x} z^t
\end{equation*}
の収束半径となる．

また，ランダム・ウォークの二点関数を $Q_p(x, t) = p^t D^{\conv t}(x) \ind{t\in\opTime}$ で定義する．
$Q_1(x, t)$ のLaplace変換として，ランダム・ウォークのGreen関数が
\begin{equation*}
  S_p(x) \defeq \sum_{t\in\opTime} Q_1(x, t) p^t = \sum_{t\in\opTime} p^t D^{\conv t}(x)
  \quad
  (x\in\opSpace,\ p\in\interval{0}{1})
\end{equation*}
で与えられる．
Boolの不等式\footnote{%
  事象列 $\{A_i\}_{i=1}^{\infty}$ に対して，$\mathbb{P}_p(\bigcup_{i=1}^{\infty} A_i) \leq \sum_{i=1}^{\infty} \mathbb{P}_p(A_i)$．%
}によって，$p\in\interval[open right]{0}{1}$ に対して $\varphi_p(x, t) \leq Q_p(x, t)$ が示される．
この不等式の両辺の和を取ることで $p_\mathrm{c}\geq 1$ が示される．

\section{主結果}

AizenmanとNewman~\cite{aizenman1984Tree}はトライアングル条件と呼ばれる十分条件を満たせば（有向でない）無向パーコレーションの臨界指数が平均場臨界指数 $\gamma=1$ に退化することを示した．
BarskyとAizenman~\cite{barsky1991Percolation}はこの条件を有向パーコレーションにも適用できるように統一的な形で再定義した．
$\norm{(x, t)}_2 = (\sum_{i=1}^{d} \abs{x_i}^2 + t^2)^{1 / 2}$ および時空間の畳み込みを $\varphi_p^{\Conv n}(x, t) = (\varphi_p^{\Conv (n-1)} \Conv \varphi_p)(x, t) \defeq \sum_{(y, s)\in\opSpaceTime} \varphi_p^{\Conv (n-1)}(y, s) \varphi_p(x - y, t - s)$ と書けば，
トライアングル条件は
\begin{equation}
  \label{eq:triangleCondition}
  \lim_{R\to\infty} \sup\Set{
    \sum_{\symbf{y}\in\opSpaceTime} \varphi_{p_\mathrm{c}}^{\Conv 2}(\symbf{y}) \varphi_{p_\mathrm{c}}(\symbf{y} - \symbf{x})
    | \norm{\symbf{x}}_2 \geq R
  }
  = 0
\end{equation}
と表される．
もし\eqref{eq:triangleCondition}が成り立てば，$\chi_p^{-2} \odv{\chi_p}/{p} = -\odv{\chi_p^{-1}}/{p}$ に注意して微分不等式
\begin{equation}
  \label{eq:differentialInequality}
  \epsilon_R \left(
    1 - \sup\Set{
      \sum_{\symbf{y}\in\opSpaceTime} \varphi_{p}^{\Conv 2}(\symbf{y}) \varphi_{p}(\symbf{y} - \symbf{x})
      | \norm{\symbf{x}}_2 \geq R
    }
  \right) \chi_p^2
  \leq \odv{\chi_p}{p}
  \leq \chi_p^2
\end{equation}
の各辺を積分することで，$\gamma=1$ が得られる．
ただし，$\epsilon_R$ は $R\gg 1$ を固定したとき $\epsilon_R \ll 1$ となるような定数である．

トライアングル条件\eqref{eq:triangleCondition}を確認するために，次の赤外評価を証明する．
これが本研究の主結果である．
なお，赤外評価が\eqref{eq:triangleCondition}を導くことは\cite{nguyen1993Triangle}を参照されたい．
\begin{theorem}[赤外評価 {[Chen, Handa and K.]}]\label{thm:infraredBound}
  単純立方格子 $\mathbb{Z}^{d\geq 183}\times\opTime$ および体心立方格子 $\mathbb{L}^{d\geq 9}\times\opTime$ 上の最近接有向パーコレーションに対して，定数 $K\in\interval[open]{0}{\infty}$ が存在して，
  \begin{equation}
    \label{eq:infraredBound}
    \abs{\flt\varphi_p(k, z)}
    \leq \frac{K}{\abs*{1 - \Napier^{\imag\arg z} \ft D(k)}}
    = K \abs{\ft S_{\Napier^{\imag\arg z}} (k)}
  \end{equation}
  が $p\in\interval[open right]{0}{p_\mathrm{c}}$ と $k\in\mathbb{T}^d$ と $\abs{z}\in\interval[open right]{1}{m_p}$ を満たす $z\in\mathbb{C}$ で一様に成り立つ．
\end{theorem}

\section{主結果の証明}

\subsection{Bootstrap argument}

定理~\ref{thm:infraredBound}の証明はbootstrap argumentと呼ばれる手法に基づく．
その詳細については\cite[Lemma~5.9]{slade2006Lacea}や\cite[Lemma~8.1]{heydenreich2017Progress}，あるいは講演者の数学総合若手研究集会に於ける過去のテクニカルレポートに譲ることとし，本稿では必要となる命題についてのみ言及する．

$k, l\in\mathbb{T}^d$ と $\abs{\mu}\in\interval{0}{1}$ を満たす $\mu\in\mathbb{C}$ に対して，
\begin{multline}
  \flt U_\mu(k, l) = \bigl(1 - \hat D(k)\bigr) \Biggl(\frac{\abs{\ft S_{\mu}(l + k)} + \abs{\ft S_{\mu}(l - k)}}{2} \abs{\ft S_{\mu}(l)}\\
  + \bigl(1 - \hat D(2l)\bigr) \abs{\ft S_{\mu}(l)} \abs{\ft S_{\mu}(l + k)} \abs{\ft S_{\mu}(l - k)}\Biggr),
\end{multline}
および $p\in\interval[open right]{0}{p_\mathrm{c}}$ と $\abs{z}\in\interval[open right]{1}{m_p}$ を満たす $z\in\mathbb{C}$ に対して，
\begin{equation*}
  \mu_p(z) = \left(1 - \flt\varphi_p(0, \abs{z})^{-1}\right) \Napier^{\imag \arg z}
\end{equation*}
とおく．
また，$\mathbb{T}^d$ 上の関数 $\ft f$ に対して，$\DLaplacian{k}\ft f(l) = \ft f(l + k) + \ft f(l - k) - 2\ft f(l)$ と書く．
以上から $\{g_i\}_{i=1}^{3}$ を
\begin{gather}
  g_1(p, m) \defeq p \max\set{1, m},\\
  g_2(p, m) \defeq \sup_{\substack{k\in\mathbb{T}^d,\\ z\in\mathbb{C}\colon\abs{z}\in\set{1, m}}}\frac{\abs*{\flt\varphi_p(k, z)}}{\abs{\flt S_{\mu_p(z)}(k)}},\\
  g_3(p, m) \defeq \sup_{\substack{k, l\in\mathbb{T}^d,\\ z\in\mathbb{C}\colon\abs{z}\in\set{1, m}}} \frac{\abs[\big]{\frac{1}{2}\DLaplacian{k}\bigl(\flt q_p(l, z) \flt\varphi_p(l, z)\bigr)}}{\flt U_{\mu_p(z)}(k, l)}
  \label{eq:op-bootstrapFunction3}
\end{gather}
によって定義する．
以下の命題は\cite[Lemma~5.9]{slade2006Lacea}の十分条件である．
これらが示されれば，各 $i=1, 2, 3$ に対して $K_i$ が存在して $g_i(p, m) < K_i$ が成り立つ，すなわち $i=2$ の上界から主結果~\ref{thm:infraredBound}が帰結される．

\begin{proposition}[連続性]\label{prp:op-continuity}
  $p\in\interval[open right]{0}{p_\mathrm{c}}$ を固定する毎に，$m\in\interval[open right]{1}{m_p}$ に関して $\{g_i\}_{i=1}^{3}$ は連続である．
  また，$p\in\interval[open right]{0}{p_\mathrm{c}}$ に関して $\{g_i(p, 1)\}_{i=1}^{3}$ は連続である．
\end{proposition}

\begin{proposition}[初期条件]\label{prp:op-initialCondition}
  或る有限な定数 $\{K_i\}_{i=1}^{3}$ が存在して，全ての $i=1, 2, 3$ に対して $g_i(0, 1)<K_i$ が成り立つ．
\end{proposition}

\begin{proposition}[Bootstrap argument]\label{prp:op-bootstrapArgument}
  $\mathbb{Z}^{d\geq 183}\times\opTime$ および $\mathbb{L}^{d\geq 9}\times\opTime$ 上の最近接有向パーコレーションに対して，$p\in\interval[open]{0}{p_\mathrm{c}}$ と $\abs{z}\in\interval[open]{1}{m_p}$ を満たす $z\in\mathbb{C}$ を固定する．
  このとき，命題~\ref{prp:op-initialCondition}と同じ $\{K_i\}_{i=1}^{3}$ に対して，
  \[
    \forall i=1, 2, 3,\quad
    \left[g_i(p, m)\leq K_i \implies g_i(p, m)<K_i\right]
  \]
  が成り立つ．
\end{proposition}

命題~\ref{prp:op-continuity}と命題~\ref{prp:op-initialCondition}の証明は\cite{chen2021Meanfield,kamijima2021Meanfield}に譲り，本稿では特に命題~\ref{prp:op-bootstrapArgument}の証明の概略についてのみ述べる．

\subsection{レース展開}

ランダム・ウォークの二点関数 $Q_p$ に対して，再生方程式
\begin{equation*}
  Q_p(\symbf{x}) = \KroneckerDelta{\symbf{o}}{\symbf{x}} + (q_p\Conv Q_p)(\symbf{x})
\end{equation*}
が成り立つことはよく知られている．
ここで，$\symbf{x}=(x, t)\in\opSpaceTime$ に対して，$\KroneckerDelta{\symbf{o}}{\symbf{x}} = \KroneckerDelta{o}{x}\KroneckerDelta{o}{t}$ とした．
レース展開は有向パーコレーションの二点関数 $\varphi_p$ に対する或る種の $Q_p$ からの摂動展開を与えるものである．
\begin{proposition}[レース展開~\cite{nguyen1993Triangle,sakai2001Meanfield}]
  任意の $p<p_\mathrm{c}$ と $N\in\nnInt$ に対して，$\opSpaceTime$ 上の非負関数 $\{\pi_p^{(n)}\}_{n=0}^{N}$ が存在して，再生方程式
  \begin{equation}
    \label{eq:op-laceExpansion}
    \varphi_p(\symbf{x}) = \KroneckerDelta{o}{\symbf{x}} + \Pi_p^{(N)}(\symbf{x})
      + \left(\bigl(\KroneckerDelta{\symbf{o}}{\bullet} + \Pi_p^{(N)}\bigr) \Conv q_p \Conv \varphi_p\right)(\symbf{x})
      + (-1)^{N+1} R_p^{(N+1)}(\symbf{x}),
  \end{equation}
  が成り立つ．ただし，
  \[
    \Pi_p^{(N)}(\symbf{x}) = \sum_{n=0}^{N}(-1)^n \pi_p^{(n)}(\symbf{x})
  \]
  であり，剰余項 $R_p^{(N+1)}(\symbf{x})$ は
  \begin{equation*}
    0 \leq R_p^{(N+1)}(\symbf{x}) \leq \left(\pi_p^{(N)} \Conv \varphi_p\right)(\symbf{x}).
  \end{equation*}
  と評価される．
\end{proposition}

下記の補題~\ref{lem:brief-diagrammaticBounds}により，$\{\pi_p^{(N)}\}_{N=1}^{\infty}$ の交代級数が絶対収束することがわかる．
ゆえに，$\symbf{x}\in\opSpaceTime$ に対して，$\lim_{N\to\infty}\pi_p^{(N)}(\symbf{x})=0$ を仮定してよい．
このとき，$N \to \infty$ で $R_p^{(N)}(\symbf{x}) \to 0$ となるから，無限級数
\begin{equation}
  \label{eq:def-sumOfLaceExpansionCoefficients}
  \Pi_p(\symbf{x}) \defeq \lim_{N\to\infty} \Pi_p^{(N)}(\symbf{x}) = \sum_{N=0}^{\infty}(-1)^N \pi_p^{(N)}(\symbf{x})
\end{equation}
はwell-definedである．

\subsection{レース展開係数の評価}

レース展開の絶対収束性を示すために，交代級数\eqref{eq:def-sumOfLaceExpansionCoefficients}の上界を得る必要がある．
$\varphi_p^{(m)}(x, t) = m^t \varphi_p(x, t)$ とおき，$\lambda, \rho\in\mathbb{N}$ と $m>0$ と $k\in\mathbb{T}^d$ に対して，
\begin{gather}
  B_{p, m}^{(\lambda, \rho)} \defeq \sup_{\symbf{x}}\sum_{\symbf{y}} \left(q_p^{\Conv \lambda} \Conv \varphi_p\right)(\symbf{y}) \left(m^\rho q_p^{\Conv \rho} \Conv \varphi_p^{(m)}\right)(\symbf{y} - \symbf{x}),
  \label{eq:def-bubble-diagram}\\
  T_{p, m}^{(\lambda, \rho)} \defeq \sup_{\symbf{x}}\sum_{\symbf{y}} \left(q_p^{\Conv \lambda} \Conv \varphi_p^{\Conv 2}\right)(\symbf{y}) \left(m^\rho q_p^{\Conv \rho} \Conv \varphi_p^{(m)}\right)(\symbf{y} - \symbf{x}),
  \label{eq:def-triangle-diagram}\\
  \flt V_{p, m}^{(\lambda, \rho)}(k) \defeq \sup_{\symbf{x}}\sum_{\symbf{y}} \left(q_p^{\Conv \lambda} \Conv \varphi_p\right)(\symbf{y}) \left(1 - \cos k\cdot y\right) \left(m^\rho q_p^{\Conv \rho} \Conv \varphi_p^{(m)}\right)(\symbf{y} - \symbf{x}).
  \label{eq:def-weighted-bubble}
\end{gather}
と定義する．
さらに，$\Pi_p(\symbf{x})$ を二つに分ける．
すなわち $\Pi_p^\mathrm{even}(\symbf{x}) = \sum_{N=1}\pi_p^{(2N)}(\symbf{x})$ および $\Pi_p^\mathrm{odd}(\symbf{x}) = \pi_p^{(1)}(\symbf{x}) - \pi_p^{(0)}(\symbf{x}) + \sum_{N=1}\pi_p^{(2N+1)}(\symbf{x})$ とする．

\begin{lemma}\label{lem:brief-diagrammaticBounds}
  $\varepsilon = \varepsilon_1^{(1)}\varepsilon_1^{(2)} \vee \varepsilon_1^{(1)}\varepsilon_2^{(2)} \vee \varepsilon_1^{(2)}\varepsilon_2^{(1)} \vee \varepsilon_2^{(1)}\varepsilon_2^{(2)} \vee (\varepsilon_1^{(1)})^3 \vee (\varepsilon_2^{(1)})^3$ とおく．
  $2 T_{p, m}^{(1, 1)} < 1$ が成り立つならば，交代級数\eqref{eq:def-sumOfLaceExpansionCoefficients}が絶対収束し，かつ
  \begin{gather*}
    \flt\Pi_p^\mathrm{even}(0, m) \leq \order{\varepsilon},\\
    \flt\Pi_p^\mathrm{odd}(0, m) \leq \frac{1}{2} B_{p, m}^{(2, 2)} + \order{\varepsilon},\\
    \sum_{(x, t)} \left(\Pi_p^\mathrm{even}(x, t) + \Pi_p^\mathrm{odd}(x, t)\right) m^t t \leq \frac{1}{2} \left(B_{p, m}^{(2, 2)} + T_{p, m}^{(2, 2)}\right) + \order{\varepsilon},\\
    \sum_{(x, t)} \left(\Pi_p^\mathrm{even}(x, t) + \Pi_p^\mathrm{odd}(x, t)\right) m^t \left(1 - \cos k\cdot x\right)
      \leq \frac{1}{2} \flt V_{p, m}^{(2, 2)}(k) + 2\flt V_{p, 1}^{(2, 2)}(k) B_{p, m}^{(0, 2)} + \order{\varepsilon}.
  \end{gather*}
  と評価できる．
\end{lemma}

この補題は\cite[Lemma~3.2]{chen2021Meanfield}あるいは\cite[Lemma~3.4.4]{kamijima2021Meanfield}の簡易版である．
誤差項 $\order{\varepsilon}$ は実際には主要項と同様に\eqref{eq:def-bubble-diagram}--\eqref{eq:def-weighted-bubble}を用いて表される．
しかしながら，数値計算を行うと，誤差項は主要項に比べて1桁以上小さい．
Bootstrap argumentを確認するに当たっては上の主張の程度でも大雑把に理解できると考えたので，本稿では簡易版を載せるに留める．
実際の数値計算の結果を見たければ，やはりプログラム等を動かす必要がある．

\subsection{Bootstrap関数の評価}

交代級数\eqref{eq:def-sumOfLaceExpansionCoefficients}が絶対収束すると仮定する．
\eqref{eq:op-laceExpansion}の両辺のFourier-Laplace変換を取ると，
\begin{equation}
  \label{eq:FourierLaplace-recursionEquation}
  \flt\varphi_p(k, z) = \frac{1 + \flt\Pi_p(k, z)}{1 - \flt q_p(k, z) \bigl(1 + \flt\Pi_p(k, z)\bigr)}.
\end{equation}
となる．この表式を用いると，$g_1$ の上界
\begin{equation}
  \label{eq:improvedBound-g1}
  g_1(p, m) \leq \frac{1}{1 + \flt\Pi_p^\mathrm{even}(0, m) - \flt\Pi_p^\mathrm{odd}(0, m)} \leq \frac{1}{1 - \flt\Pi_p^\mathrm{odd}(0, m)}
\end{equation}
が得られる．
補題~\ref{lem:brief-diagrammaticBounds}で評価された量が現れていることに注目されたい．
また，\cite[Lemma~5.7]{slade2006Lacea}を若干修正した補題\cite[Lemma~4.4]{chen2021Meanfield}を用いることによって，$g_3$ の上界
\begin{multline}
  g_3(p, m)
  \leq \max\Biggl\{ 1, \frac{g_2(p, m)}{1 - \flt\Pi_p^\mathrm{even}(0, m) - \flt\Pi_p^\mathrm{odd}(0, m)}\Biggr\}^3 K_1^2\\
  \times \Biggl(
    1 + 2 \left(\flt\Pi_p^\mathrm{even}(0, m) + \flt\Pi_p^\mathrm{odd}(0, m)\right)\\
    + 2 \norm*{
      \frac{
        \bigl(\flt\Pi_p^\mathrm{even}(0, m) + \flt\Pi_p^\mathrm{odd}(0, m)\bigr)
        - \bigl(\flt\Pi_p^\mathrm{even}(\bullet, m) + \flt\Pi_p^\mathrm{odd}(\bullet, m)\bigr)
      }{
        1 - \ft D(\bullet)
      }
    }_\infty
  \Biggr)^2
  \label{eq:improvedBound-g3}
\end{multline}
も得られる．
これらは単純立方格子 $\mathbb{Z}^d\times\opTime$ でも体心立方格子 $\mathbb{L}^d\times\opTime$ でも成り立つ．

一方で，$g_2$ の上界はそれぞれの格子で変わる上に，次の命題と補題を必要とする：

\begin{proposition}\label{prp:restricting-g2}
  体心立方格子 $\mathbb{L}^d$ 上では，$g_2(p, m)$ の上限を計算するために $k$ の範囲を $\interval{-\pi / 2}{\pi / 2}^d$ にすれば十分である．すなわち，
  \[
    g_2(p, m)
    = \sup_{\substack{k\in\mathbb{T}^d,\\ z\in\mathbb{C}\colon\abs{z}\in\Set{1, m}}}\frac{\abs{\flt\varphi_p(k, z)}}{\abs{\ft S_{\mu_p(z)}(k)}}
    = \sup_{\substack{k\in\interval{-\pi / 2}{\pi / 2}^d,\\ z\in\mathbb{C}\colon\abs{z}\in\Set{1, m}}}\frac{\abs{\flt\varphi_p(k, z)}}{\abs{\ft S_{\mu_p(z)}(k)}}.
  \]
\end{proposition}

\begin{lemma}  \label{lem:upperBound-Green}
  不等式 $\ft D(k) \geq 0$ を満たす任意の $k\in\mathbb{T}^d$，および任意の $r\in\interval{0}{1}$ と $\theta\in\mathbb{T}$ に対して，
  \[
    \abs*{1 - r \Napier^{\imag\theta} \ft D(k)} \geq \frac{1}{2} \left(\frac{\abs{\theta}}{\pi} + 1 - \ft D(k)\right).
  \]
\end{lemma}

これらを用いると，
\begin{align}
  g_2(p, m)
  \leq {} & \frac{1 + \flt\Pi_p^\mathrm{even}(0, m) + \flt\Pi_p^\mathrm{odd}(0, m)}{1 - \flt\Pi_p^\mathrm{odd}(0, m)}
    + \frac{K_2}{1 - \flt\Pi_p^\mathrm{odd}(0, m)}
    \Biggl(
      2 \flt\Pi_p^\mathrm{even}(0, m)\notag\\
      & + c K_1 \max\biggl\{
        \pi \sum_{(x, t)} \Bigl(
          \Pi_p^\mathrm{even}(x, t) + \Pi_p^\mathrm{odd}(x, t)
        \Bigr)
        m^t t,
      \notag\\
      & \quad
        \sum_{(x, t)} \Bigl(
          \Pi_p^\mathrm{even}(x, t) + \Pi_p^\mathrm{odd}(x, t)
        \Bigr)
        m^t \frac{1 - \cos k\cdot x}{1 - \ft D(k)}
      \biggr\}
    \Biggr)
  \label{eq:improvedBound-g2}
\end{align}
が得られる．ただし，単純立方格子，体心立方格子それぞれに対して $c=4, 2$ である．
命題~\ref{prp:restricting-g2}と補題~\ref{lem:upperBound-Green}は先行研究\cite{nguyen1993Triangle}に無く，講演者らは新たに証明する必要があった．
詳しくは\cite[Remark~2]{chen2021Meanfield}を参照されたい．
体心立方格子上では命題~\ref{prp:restricting-g2}で $k$ の範囲を縮めていたのに対し，単純立方格子 $\mathbb{Z}^d$ 上では $\theta$ の範囲を縮めることができる (関連する論文\cite{kamijima2022Meanfield}を現在執筆中である)．
これに伴って，補題~\ref{lem:upperBound-Green}も異なるものを使う必要があるため，上の定数 $c$ の違いが現れる．

\subsection{ランダム・ウォーク量によるダイアグラムの評価}

上で定義した $B_{p, m}^{(\lambda, \rho)}$，$T_{p, m}^{(\lambda, \rho)}$ および $V_{p, m}^{(\lambda, \rho)}(k)$ は次の補題のように評価できる．
これらの上界は（単なる定数を除いて）$\{K_i\}_{i=1}^{3}$ とランダム・ウォーク量\eqref{eq:def-rwQuantities}のみで表されていることに注目されたい．

\begin{lemma} \label{lem:basic-diagrams}
  $g_i(p, m)\leq K_i$, $i=1, 2, 3$ を仮定する．
  各 $\mu, \rho\in\mathbb{N}$ と $p\in\interval[open right]{0}{p_\mathrm{c}}$ と $m\in\interval[open right]{1}{m_p}$ に対して，
  \begin{gather}
    \label{eq:bubble-diagram}
    B_{p, m}^{(\lambda, \rho)} \leq K_1^{\lambda + \rho} K_2^2 \varepsilon_1^{(\floor{(\lambda + \rho) / 2})},\\
    \label{eq:triangle-diagram}
    T_{p, m}^{(\lambda, \rho)} \leq \sqrt{2} K_1^{\lambda + \rho} K_2^3 \varepsilon_2^{(\floor{(\lambda + \rho) / 2})},\\
    \label{eq:weighted-bubble}
    \norm*{\frac{\flt V_{p, m}^{(\lambda, \rho)}}{1 - \ft D}}_\infty \leq \begin{dcases}
      K_1^2 \norm*{D}_\infty + K_1^3 K_2 \sqrt{\varepsilon_1^{(2)}} + \norm*{\frac{\flt V_{p, m}^{(2, 1)}}{1 - \ft D}}_\infty& [\lambda = \rho = 1],\\
      \begin{multlined}
        \lambda \left(\lambda - 1\right) K_1^{\lambda + \rho} K_2^2 \varepsilon_1^{(\floor{(\lambda + \rho - 1) / 2})}\\
        + \lambda K_1^{\lambda + \rho - 1} K_2 K_3 \bigl(\sqrt{2} + 4\bigr) \varepsilon_2^{(\floor{(\lambda + \rho - 1) / 2})}
      \end{multlined} & [\lambda \geq 2\text{ or }\rho \geq 2].
    \end{dcases}
  \end{gather}
\end{lemma}

\subsection{命題~\ref{prp:op-bootstrapArgument}の証明}

以上の評価を合わせると，命題~\ref{prp:op-bootstrapArgument}が証明できる．
体心立方格子の場合についてのみ示す（単純立方格子でも数値が変わる程度で同様）．
具体的に
\[
  d = \num{9}, \quad
  K_1 = \num{1.0020}, \quad
  K_2 = \num{1.0500}, \quad
  K_3 = \num{1.2500},
\]
を代入する（これらは命題~\ref{prp:op-initialCondition}を満たすように取る）．
補題~\ref{lem:basic-diagrams}と表~\ref{tbl:numericalValues-rw-bcc}によって，
\begin{gather*}
  \begin{aligned}
    B_{p, m}^{(2, 2)} &\leq \num{2.11688e-4},&
    B_{p, m}^{(0, 2)} &\leq \num{2.37279e-3},\\
    T_{p, m}^{(2, 2)} &\leq \num{4.40247e-4},&
    T_{p, m}^{(1, 1)} &\leq \num{3.96190e-3},
  \end{aligned}\\
  \frac{\flt V_{p, m}^{(2, 2)}(k)}{1 - \ft D(k)} \leq \num{3.92276e-2}
\end{gather*}
を得る．
この内，4番目の不等式は補題~\ref{lem:brief-diagrammaticBounds}の十分条件であることに注意されたい．
さらに，補題~\ref{lem:brief-diagrammaticBounds}によって，
\begin{gather*}
  \flt\Pi_p^\mathrm{even}(0, m) \leq \num{1.00000e-5},\\
  \flt\Pi_p^\mathrm{odd}(0, m) \leq \num{1.15844e-4},\\
  \sum_{(x, t)} \left(\Pi_p^\mathrm{even}(x, t) + \Pi_p^\mathrm{odd}(x, t)\right) m^t t
    \leq \num{4.00000e-4},\\
  \sum_{(x, t)} \left(\Pi_p^\mathrm{even}(x, t) + \Pi_p^\mathrm{odd}(x, t)\right) m^t \frac{1 - \cos k\cdot x}{1 - \ft D(k)}
    \leq \num{2.10000e-2}
\end{gather*}
となるから，交代級数\eqref{eq:def-sumOfLaceExpansionCoefficients}が絶対収束する．
最後に，\eqref{eq:improvedBound-g1}と\eqref{eq:improvedBound-g2}と\eqref{eq:improvedBound-g3}によって，
\begin{align*}
  g_1(p, m) &\leq \num{1.0002} < K_1,&
  g_2(p, m) &\leq \num{1.0445} < K_2,&
  g_3(p, m) &\leq \num{1.2433} < K_3
\end{align*}
を得る．これは $d=9$ のみでbootstrap argumentを示したに過ぎない．しかし，ランダム・ウォーク量\eqref{eq:def-rwQuantities}は次元 $d$ に関して単調減少なので，補題~\ref{lem:basic-diagrams}の上界は同じ $\{K_i\}_{i=1}^{3}$ の数値かつ $d>9$ に対しても成り立つ．
したがって，$d\geq 9$ で命題~\ref{prp:op-bootstrapArgument}が成り立つ．\qed

\printbibliography[title=参考文献]

\end{document}
