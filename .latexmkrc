#!/user/bin/env perl

ensure_path('TEXINPUTS', 'tikz-lace');
$lualatex = 'lualatex %O -kanji=utf8 -no-guess-input-env -synctex=1 -halt-on-error -interaction=nonstopmode -file-line-error %S';
$biber = 'biber --bblencoding=utf8 -u -U --output_safechars';
if ($^O eq 'MSWin32') {
	#$pdf_previewer = '"SumatraPDF -reuse-instance';
  $pdf_previewer = 'texstudio';
} elsif ($^O eq 'darwin') {
	$pdf_previewer = 'open -a Preview %S';
} elsif ($^O eq 'linux') {
	#$pdf_previewer = 'evince';
	$pdf_previewer = 'okular';
}
$max_repeat = 5;
$pdf_mode = 4;
#$pdf_update_method = 4;
#$preview_continuous_mode = 1;
#$pvc_view_file_via_temporary = 0;
$clean_ext = "nav snm bbl run.xml";
